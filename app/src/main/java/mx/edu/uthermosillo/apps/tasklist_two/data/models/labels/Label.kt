package mx.edu.uthermosillo.apps.tasklist_two.data.models.labels

import com.google.gson.annotations.SerializedName

data class Label(
    @SerializedName("id") var id: Int,
    @SerializedName("name") var name: String,
    @SerializedName("color") var color: String
)

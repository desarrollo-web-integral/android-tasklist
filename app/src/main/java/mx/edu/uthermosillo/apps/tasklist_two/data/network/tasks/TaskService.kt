package mx.edu.uthermosillo.apps.tasklist_two.data.network.tasks

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import mx.edu.uthermosillo.apps.tasklist_two.core.RetrofitHelper
import mx.edu.uthermosillo.apps.tasklist_two.data.models.tasks.Task

class TaskService {

    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getTasks() : List<Task> {

        return withContext(Dispatchers.IO) {
            val response = retrofit.create(
                TaskApiClient::class.java)
                .getAllTasks()
            response.body() ?: emptyList()
        }
    }

    suspend fun getTask(task_id: Int) : Task {

        return withContext(Dispatchers.IO) {
            val response = retrofit.create(
                TaskApiClient::class.java)
                .getTask(task_id)
            response.body()!!
        }
    }
}
package mx.edu.uthermosillo.apps.tasklist_two.domain.tasks

import mx.edu.uthermosillo.apps.tasklist_two.data.repositories.TasksRepository

class GetAllTasks {

    private val repository = TasksRepository()

    suspend operator fun invoke() = repository.getAllTask()
}
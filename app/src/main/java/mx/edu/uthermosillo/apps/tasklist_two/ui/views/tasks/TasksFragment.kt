package mx.edu.uthermosillo.apps.tasklist_two.ui.views.tasks

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import mx.edu.uthermosillo.apps.tasklist_two.R
import mx.edu.uthermosillo.apps.tasklist_two.databinding.FragmentTasksBinding
import mx.edu.uthermosillo.apps.tasklist_two.ui.viewmodels.TasksViewModel
import mx.edu.uthermosillo.apps.tasklist_two.ui.views.adapters.TasksAdapter

class TasksFragment : Fragment() {

    private var _binding: FragmentTasksBinding? = null
    private val binding get() = _binding!!

    private val tasksViewModel : TasksViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        _binding = FragmentTasksBinding.inflate(inflater, container, false)

        tasksViewModel.loadTasks()

        tasksViewModel.tasks.observe(viewLifecycleOwner, Observer {
            tasks ->
            binding.tasksRecycler.layoutManager = LinearLayoutManager(context)
            val adapter = TasksAdapter(tasks)
            binding.tasksRecycler.adapter = adapter

            if (tasks.isNotEmpty()) {
                binding.tasksShimmer.visibility = View.GONE
                binding.tasksRecycler.visibility = View.VISIBLE
            }
        })

        tasksViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.tasksShimmer.isVisible = it
        })

        return binding.root
    }
}
package mx.edu.uthermosillo.apps.tasklist_two.data.network.tasks

import mx.edu.uthermosillo.apps.tasklist_two.data.models.tasks.Task
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface TaskApiClient {
    @GET("tasks")
    suspend fun getAllTasks() : Response<List<Task>>

    @GET("tasks/{id}")
    suspend fun getTask(@Path("id") id: Int) : Response<Task>
}
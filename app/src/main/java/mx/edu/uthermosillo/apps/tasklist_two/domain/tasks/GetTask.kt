package mx.edu.uthermosillo.apps.tasklist_two.domain.tasks

import mx.edu.uthermosillo.apps.tasklist_two.data.repositories.TasksRepository

class GetTask {

    private val repository = TasksRepository()

    suspend operator fun invoke(task_id: Int) =
        repository.getTask(task_id)
}
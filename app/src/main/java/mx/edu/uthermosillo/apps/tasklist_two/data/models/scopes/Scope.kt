package mx.edu.uthermosillo.apps.tasklist_two.data.models.scopes

data class Scope(
    var id: Int,
    var name: String,
    var description: String
)

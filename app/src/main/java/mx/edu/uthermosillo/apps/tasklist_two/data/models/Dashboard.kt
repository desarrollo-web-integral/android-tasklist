package mx.edu.uthermosillo.apps.tasklist_two.data.models

data class Dashboard(
    var id: Int,
    var name: String,
    var image: String,
)

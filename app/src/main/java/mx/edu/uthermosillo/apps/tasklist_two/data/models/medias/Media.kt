package mx.edu.uthermosillo.apps.tasklist_two.data.models.medias

import com.google.gson.annotations.SerializedName

data class Media(
    @SerializedName("id") var id: Int,
    @SerializedName("name") var name: String,
    @SerializedName("image") var image: String
)

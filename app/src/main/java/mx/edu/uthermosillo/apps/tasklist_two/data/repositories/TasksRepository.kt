package mx.edu.uthermosillo.apps.tasklist_two.data.repositories

import mx.edu.uthermosillo.apps.tasklist_two.data.models.tasks.Task
import mx.edu.uthermosillo.apps.tasklist_two.data.models.tasks.TaskProvider
import mx.edu.uthermosillo.apps.tasklist_two.data.models.tasks.TasksProvider
import mx.edu.uthermosillo.apps.tasklist_two.data.network.tasks.TaskService

class TasksRepository {

    private val api = TaskService()

    suspend fun getAllTask() : List<Task> {
        val response = api.getTasks()
        TasksProvider.tasks = response
        return response
    }

    suspend fun getTask(task_id: Int) : Task {
        val response = api.getTask(task_id)
        TaskProvider.task = response
        return response
    }

}
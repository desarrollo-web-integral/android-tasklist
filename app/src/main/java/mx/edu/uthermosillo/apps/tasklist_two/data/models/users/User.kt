package mx.edu.uthermosillo.apps.tasklist_two.data.models.users

import mx.edu.uthermosillo.apps.tasklist_two.data.models.levels.Level

data class User(
    var id: Int,
    var name: String,
    var email: String,
    var password: String,
    var phone: String,
    var status: String,
    var level: Level
)

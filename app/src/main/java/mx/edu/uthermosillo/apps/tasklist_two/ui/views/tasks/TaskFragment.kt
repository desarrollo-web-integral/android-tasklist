package mx.edu.uthermosillo.apps.tasklist_two.ui.views.tasks

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.squareup.picasso.Picasso
import mx.edu.uthermosillo.apps.tasklist_two.R
import mx.edu.uthermosillo.apps.tasklist_two.databinding.FragmentTaskBinding
import mx.edu.uthermosillo.apps.tasklist_two.ui.viewmodels.TasksViewModel

class TaskFragment : Fragment() {

    private var _binding: FragmentTaskBinding? = null
    private val binding get() = _binding!!

    private val tasksViewModel : TasksViewModel by viewModels()
    private var taskId = 0
    private val args: TaskFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        _binding = FragmentTaskBinding.inflate(inflater, container, false)
        taskId = args.taskId

        tasksViewModel.loadTask(taskId)

        tasksViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.tasksShimmer.isVisible = it
            binding.content.isVisible = !it
        })

        tasksViewModel.task.observe(viewLifecycleOwner, Observer {
            task ->
            binding.description.text = task.description

            Picasso.get().load(task.media.image).into(binding.taskImage)
        })

        return binding.root
    }
}
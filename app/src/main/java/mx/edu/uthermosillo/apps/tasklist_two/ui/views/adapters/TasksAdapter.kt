package mx.edu.uthermosillo.apps.tasklist_two.ui.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import mx.edu.uthermosillo.apps.tasklist_two.R
import mx.edu.uthermosillo.apps.tasklist_two.data.models.tasks.Task
import mx.edu.uthermosillo.apps.tasklist_two.databinding.ItemTaskBinding
import mx.edu.uthermosillo.apps.tasklist_two.ui.views.tasks.TasksFragmentDirections

class TasksAdapter(private val tasks : List<Task>) :
    RecyclerView.Adapter<TasksAdapter.TasksViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TasksViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return TasksViewHolder(layoutInflater.inflate(
            R.layout.item_task,
        parent, false), parent.context)
    }

    override fun onBindViewHolder(holder: TasksViewHolder, position: Int) {
        holder.render(tasks[position])

        val task : Task = tasks[position]
        val binding = ItemTaskBinding.bind(holder.itemView)

        binding.textView.text = task.description
        Picasso.get().load(task.media.image).into(binding.imageView)
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    class TasksViewHolder(view: View, ct: Context) :
        RecyclerView.ViewHolder(view) {

        private val binding = ItemTaskBinding.bind(view)
        private val context = ct

        fun render(task: Task) {

            binding.taskCard.setOnClickListener {
                //Toast.makeText(context, "Brand :" + vehicle.brand.name, Toast.LENGTH_SHORT).show()
                val navController : NavController =
                    Navigation.findNavController(binding.root)

                val action =
                    TasksFragmentDirections.actionTasksFragmentToTaskFragment(task.id)
                navController.navigate(action)
            }
        }
    }
}
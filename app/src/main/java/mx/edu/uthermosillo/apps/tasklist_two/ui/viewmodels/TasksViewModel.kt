package mx.edu.uthermosillo.apps.tasklist_two.ui.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import mx.edu.uthermosillo.apps.tasklist_two.data.models.tasks.Task
import mx.edu.uthermosillo.apps.tasklist_two.domain.tasks.GetAllTasks
import mx.edu.uthermosillo.apps.tasklist_two.domain.tasks.GetTask

class TasksViewModel : ViewModel() {
    val tasks = MutableLiveData<List<Task>>()
    val task = MutableLiveData<Task>()
    val isLoading = MutableLiveData<Boolean>()

    var getAllTasks = GetAllTasks()
    var getTask = GetTask()

    fun loadTask(task_id: Int) {
        viewModelScope.launch {
            val result = getTask.invoke(task_id)
            isLoading.postValue(true)
            task.postValue(result)
            isLoading.postValue(false)
        }
    }

    fun loadTasks() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getAllTasks()

            if(!result.isNullOrEmpty()) {
                tasks.postValue(result)
                isLoading.postValue(false)
            }
        }
    }
}
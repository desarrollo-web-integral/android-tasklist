package mx.edu.uthermosillo.apps.tasklist_two.data.models.tasks

import com.google.gson.annotations.SerializedName
import mx.edu.uthermosillo.apps.tasklist_two.data.models.medias.Media
import mx.edu.uthermosillo.apps.tasklist_two.data.models.scopes.Scope

data class Task(
    @SerializedName("id") val id: Int,
    @SerializedName("scope") val scope: Scope,
    @SerializedName("media") val media: Media,
    @SerializedName("title") val title: String,
    @SerializedName("description") val description: String,
    @SerializedName("due_date") val due_date: String,
    @SerializedName("status") val status: Int
)

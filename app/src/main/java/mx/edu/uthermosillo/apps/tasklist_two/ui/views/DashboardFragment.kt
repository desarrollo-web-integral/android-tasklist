package mx.edu.uthermosillo.apps.tasklist_two.ui.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import mx.edu.uthermosillo.apps.tasklist_two.data.models.Dashboard
import mx.edu.uthermosillo.apps.tasklist_two.databinding.FragmentDashboardBinding
import mx.edu.uthermosillo.apps.tasklist_two.ui.views.adapters.DashboardAdapter

class DashboardFragment : Fragment() {

    private var _binding: FragmentDashboardBinding? = null
    private val binding get() = _binding!!

    private lateinit var dashboardAdapter: DashboardAdapter
    private var options: List<Dashboard> = emptyList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        loadOptions()

        val recyclerView = binding.dashboardRecycler
        dashboardAdapter = DashboardAdapter(options)

        val layoutManager = LinearLayoutManager(context)
        //val layoutManager = GridLayoutManager(context, 2)

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = dashboardAdapter

        return binding.root
    }

    private fun loadOptions() {

        options = listOf(
            Dashboard(1, "Tasks", "https://res.cloudinary.com/dbvz6c7ki/image/upload/t_pen_final/v1701913225/pen_nomqmi.jpg"),
            Dashboard(2, "Scopes", "https://res.cloudinary.com/dbvz6c7ki/image/upload/t_aur/v1701913124/telescope_ntakhi.jpg"),
            Dashboard(3, "Medias", "https://res.cloudinary.com/dbvz6c7ki/image/upload/t_aur/v1701913007/aurora_v8yw5y.jpg"),
            Dashboard(4, "Users", "https://res.cloudinary.com/dbvz6c7ki/image/upload/t_scope_2/v1701912910/users_goxy23.jpg"),
            Dashboard(5, "Levels", "https://res.cloudinary.com/dbvz6c7ki/image/upload/t_pen_2/v1701912290/pexels-pixabay-260024_knl91r.jpg"),
        )
    }


}
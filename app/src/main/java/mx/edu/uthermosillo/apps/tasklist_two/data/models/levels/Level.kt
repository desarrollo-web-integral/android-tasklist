package mx.edu.uthermosillo.apps.tasklist_two.data.models.levels

import com.google.gson.annotations.SerializedName

data class Level(
    @SerializedName("id") var id: Int,
    @SerializedName("code") var code: String,
    @SerializedName("name") var name: String,
    @SerializedName("status") var status: Int,
    @SerializedName("description") var description: String
)

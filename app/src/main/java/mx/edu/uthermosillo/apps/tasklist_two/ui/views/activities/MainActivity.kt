package mx.edu.uthermosillo.apps.tasklist_two.ui.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import mx.edu.uthermosillo.apps.tasklist_two.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}